#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  QObject::connect(ui->actionNew,&QAction::triggered,
                   this,&MainWindow::NewText);
  QObject::connect(ui->actionUpdate_DataBase,&QAction::triggered,
                   this,&MainWindow::WriteDataIntoFile);
  QObject::connect(ui->actionUpdate_RAM_DataBase,&QAction::triggered,
                   this,&MainWindow::ReadDataFromFile);
  QObject::connect(ui->actionExit,SIGNAL(triggered(bool)),
                   this,SLOT(close()));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_pushButton_clicked()
{
  QString NewName = ui->NAME->text() + " <|> ";
  NewName += ui->FAMILY->text()+ '\n';
  ui->FULLTEXT->setText(ui->FULLTEXT->toPlainText() + NewName);
  ui->NAME->setText(QString());
  ui->FAMILY->setText(QString());
}

void MainWindow::NewText(){
  ui->FULLTEXT->setText(QString());
}

void MainWindow::WriteDataIntoFile(){
  bool state;
  MainWindow::FileName = QInputDialog::getText(this,"Set File Name",
                                               "Enter File Name :",QLineEdit::Normal
                                               ,QString(),&state);
  if(state && !FileName.isEmpty()){
      QFile filePos(MainWindow::FileName);
      QTextStream stream(&filePos);
      filePos.open(QIODevice::WriteOnly | QIODevice::Text);
      stream << ui->FULLTEXT->toPlainText();
      filePos.close();
    }
}

void MainWindow::ReadDataFromFile(){
  if(MainWindow::FileName.isEmpty()){
      QMessageBox::information(this,"Alert","Before Read, u\nmost save data",
                               QMessageBox::Ok);
      return;
    }
  QFile filePos(MainWindow::FileName);
  filePos.open(QIODevice::ReadOnly);
  ui->FULLTEXT->setText(filePos.readAll());
  filePos.close();
}
