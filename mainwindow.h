#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void on_pushButton_clicked();
  void NewText();
  void WriteDataIntoFile();
  void ReadDataFromFile();
private:
  Ui::MainWindow *ui;
  QString FileName;
};

#endif // MAINWINDOW_H
